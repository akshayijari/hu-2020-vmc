import React from 'react';
import './App.css';
import { regionTypes } from './constants/constants';
import { ImageList } from './components/PageOne/index';
import { InstancePage } from './components/PageTwo/index'
import { ResultPage, UpdatePageFive } from './components/PageFive/index'
import { StorageAndNetwork } from './components/PageThree/index'


const App = () => {

  interface ImageDataProps {
    pageName: string;
  };

  const [region, setRegion] = React.useState('Region');
  const [pagename, setPageName] = React.useState('Choose Image');

  const [pageOneData, setPageoneData] = React.useState([""]);
  const [pageTwoData, setPageTwoData] = React.useState([""]);

  const [cost1, setCost1] = React.useState(0);
  const [cost2, setCost2] = React.useState(0);
  const [cost3, setCost3] = React.useState(0);
  const [cost4, setCost4] = React.useState(0);

  



  const onregionChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    setRegion(event.target.value);
  };


  const setheader = (pagename: string) => {
    setPageName(pagename);
    return pagename;
  };

  const ponecostdetails = (data: number) => {
    setCost1(data);
  }

  const pageoneDetails = (data: string[]) => {
    setPageoneData(data)
  };

  const ptwoDetails = (data: string[]) => {
    setPageTwoData(data);
  }

  const ptwoCost = (cost: number) => {
    setCost2(cost);
  }

  return (

    <div className="App">
      <div className="App-header">
        <div className="hvc">HVC</div>
      </div>

      <div className="divide">
        <div className="left-section">

          <div className="secondary-header">
            <p className="header-name" >{pagename}</p>
            <button>
            <select role="RegionMenu" className="region-select" onChange={onregionChange} value={region}>
              {regionTypes.map((region: string) => (
                <option  value={region}>{region}</option>
              ))
              }
            </select>
            </button>
            
            <div className="screen-name">
              <button onClick={() => { setheader("Choose Image") }} className={pagename === "Choose Image" ? 'selected' : 'pageone'}>1.Choose Image</button>
              <button onClick={() => { setheader("Choose Instance Type") }} className={pagename === "Choose Instance Type" ? 'selected' : 'pagetwo'}>2.Choose Instance Type</button>
              <button onClick={() => { setheader("Choose Storage and Network") }} className={pagename === "Choose Storage and Network" ? 'selected' : 'pagethree'}>3.Choose Storage and Network</button>
              <button onClick={() => { setheader("Configure Security") }} className={pagename === "Configure Security" ? 'selected' : 'pagefour'}>4.Configure Security</button>
              <button onClick={() => { setheader("Review and Launch") }} className={pagename === "Review and Launch" ? 'selected' : 'pagefive'}>5.Review and Launch</button>
            </div>
           
          </div>
          <div className="section-body">
            {pagename === "Choose Image" && <ImageList region={region} ponecost={(cost) => { ponecostdetails(cost) }} POnefunction={(data) => { pageoneDetails(data) }} />}
            {pagename === "Choose Instance Type" && <InstancePage ptwodata={(data) => { ptwoDetails(data) }} cost2={(cost) => { ptwoCost(cost) }} />}
            {pagename === "Choose Storage and Network" && <StorageAndNetwork InstanceType = {pageTwoData[2]} />}
            {pagename === "Review and Launch" && <ResultPage pageone={pageOneData} cost1={cost1} pagetwo={pageTwoData} cost2={cost2} />}

          </div>
        </div>

        <div className="right-section">
          <div className="costHeader">Cost Estimates </div>
          <div className="costList">
            <div className="pageonecost">{pageOneData[0]} </div>
            <div className="pageonecost">{cost1}/month</div>
            <div className="pageonecost">{pageTwoData[0]} {pageTwoData[1]}</div>
            <div className="pageonecost">{cost2}/month</div>

          </div>
          <footer>Total Cost = ${cost1 + cost2 + cost3 + cost4}/month</footer>
        </div>
        <div>
          <UpdatePageFive pageone={pageOneData} cost1={cost1} pagetwo={pageTwoData} cost2={cost2} />
        </div>
      </div>
    </div>
  );
}

export default App;
