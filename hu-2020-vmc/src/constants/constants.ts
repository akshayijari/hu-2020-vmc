export const regionTypes = ['Region', 'US-East-1', 'US-East-2', 'US-West-1', 'India-1'];

export const GeneralCore = ['CPU Cores','1core', '2core', '4core'];

export const GeneralMemory = ['Memory','256MB','512MB','1GB','2GB','4GB'];

export const CPUCore = ['CPU Courses','1core', '8core', '16core'];

export const CPUMemory = ['Memory','16GB','32GB','64GB'];

export const StorageCore = ['CPU Courses','1core', '2core', '8core', '16core'];

export const StorageMemory = ['Memory','16GB','32GB','64GB'];

export const NetworkCore = ['CPU Courses','1core', '2core', '4core', '8core', '16core'];

export const NetworkMemory = ['Memory','256MB','512MB','1GB','2GB','4GB','16GB','32GB','64GB'];

export const Type = ['Storage','Magnetic Disk','SSD']