import * as React from 'react';
import './style.css';
import * as DropDown from '../../constants/constants';
interface IPageThreeProps {
    InstanceType: string;
}

export const StorageAndNetwork: React.FC<IPageThreeProps> = (props: IPageThreeProps) => {

    const [dropdownlabel, setDropdownLabel] = React.useState("Storage");
    const [storageType, setStorageType ] = React.useState("Storage")

    const onTypeChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        setStorageType(event.target.value);
    }

    return (

        <div className="main">
            <div className="p3body">
                <div className="p3card">
                    <div>Type</div>
                    <div>Volume</div>
                    <div>Capacity</div>
                    <div>Encryption</div>
                    <div>IOPS</div>
                    <div>Backup Required</div>
                    <div>Remarks</div>
                    <div>
                        <select className="storage-type" onChange={onTypeChange} value={storageType}>
                            {DropDown.Type.map((storageType: string) => (
                                <option value={storageType}>{storageType}</option>
                            ))
                            }
                        </select>
                    </div>
                </div>
            </div>
                <div className="slidecontainer">
                Network Bandwidth Configuration
                    <input className="slider" type="range" min="1" max="100" value="50" ></input>
                </div>
        </div>



    );
}