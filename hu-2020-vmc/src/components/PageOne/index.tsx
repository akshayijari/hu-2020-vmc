import * as React from 'react';
import './style.css';

interface IImageListProps {
    region: string;
    ponecost :(cost : number) => void;
    POnefunction: (data: string[]) => void;
}



export const ImageList: React.FC<IImageListProps> = (props: IImageListProps) => {
    
    const [cardDetails, setCardDetails] = React.useState([""]);
    const [processorLabel, setProcessorLabel] = React.useState("");
    
    const onRadioClick = (processorLabel: string) => {
        setProcessorLabel(processorLabel);
    }

    const onSelect = (imageName: string, processor: string) => {
        { imageName === "Linux 2 Image"&& props.ponecost(243.61) }
        { imageName === "Ubuntu Server 18.08 LTS"  && props.ponecost(243.61) }
        { imageName === "Red Hat Enterprise Linux 8" && props.ponecost(300.00) }
        { imageName === "Microsoft Windows Server 2019 Base" && props.ponecost(338.77) }
        { imageName === "SUSE Linux Enterprise Server" && props.ponecost(200.22) }
        setCardDetails([imageName, processor]);
    }



    return (
        <div className="main">
            <div className="body">
                <div className="linux">
                    <div className="img"></div>
                    <div className="img-body">
                        <h1>linux 2 Image</h1>
                        <p>Linux 2 comes with the 5years of support.It provides the linux kernal 4.14 tuned for optimal perfrmance</p>
                    </div>
                    <div className="img-processor">
                        <div><input type="radio" name="img-processor" onClick={() => { onRadioClick("x86") }} /> 64-bit (x86)</div>
                        <div><input type="radio" name="img-processor" onClick={() => { onRadioClick("ARM") }} /> 64-bit (ARM)</div>
                        <button onClick={() => { onSelect("Linux 2 Image", processorLabel) }} className="select-button">select</button>
                    </div>
                </div>
                <div className="ubuntu">
                    <div className="img"></div>
                    <div className="img-body">
                        <h1>Ubuntu Server 18.08 LTS</h1>
                        <p>Linux 2 comes with the 5years of support.It provides the linux kernal 4.14 tuned for optimal perfrmance</p>
                    </div>
                    <div className="img-processor">
                        <div><input type="radio" name="img-processor" onClick={() => { onRadioClick("x86") }} /> 64-bit (x86)</div>
                        <div><input type="radio" name="img-processor" onClick={() => { onRadioClick("ARM") }} /> 64-bit (ARM)</div>
                        <button onClick={() => { onSelect("Ubuntu Server 18.08 LTS", processorLabel) }} className="select-button">select</button>
                    </div>
                </div>
                <div className="red-hat">
                    <div className="img"></div>
                    <div className="img-body">
                        <h1>Red Hat Enterprise Linux 8</h1>
                        <p>Linux 2 comes with the 5years of support.It provides the linux kernal 4.14 tuned for optimal perfrmance</p>
                    </div>
                    <div className="img-processor">
                        <div><input type="radio" name="img-processor" onClick={() => { onRadioClick("x86") }} /> 64-bit (x86)</div>
                        <div><input type="radio" name="img-processor" onClick={() => { onRadioClick("ARM") }} /> 64-bit (ARM)</div>
                        <button onClick={() => { onSelect("Red Hat Enterprise Linux 8", processorLabel) }} className="select-button">select</button>
                    </div>

                </div>
                {(props.region === "US-East-1")  &&
                    <div className="microsoft">
                        <div className="img"></div>
                        <div className="img-body">
                            <h1>Microsoft Windows Server 2019 Base</h1>
                            <p>Linux 2 comes with the 5years of support.It provides the linux kernal 4.14 tuned for optimal perfrmance</p>
                        </div>
                        <div className="img-processor">
                            <div><input type="radio" name="img-processor" onClick={() => { onRadioClick("x86") }} /> 64-bit (x86)</div>
                            <div></div>
                            <button onClick={() => { onSelect("Microsoft Windows Server 2019 Base", processorLabel) }} className="select-button">select</button>
                        </div>

                    </div>
                }
                {(props.region === "US-East-2")  &&
                    <div className="microsoft">
                        <div className="img"></div>
                        <div className="img-body">
                            <h1>Microsoft Windows Server 2019 Base</h1>
                            <p>Linux 2 comes with the 5years of support.It provides the linux kernal 4.14 tuned for optimal perfrmance</p>
                        </div>
                        <div className="img-processor">
                            <div><input type="radio" name="img-processor" onClick={() => { onRadioClick("x86") }} /> 64-bit (x86)</div>
                            <div></div>
                            <button onClick={() => { onSelect("Microsoft Windows Server 2019 Base", processorLabel) }} className="select-button">select</button>
                        </div>

                    </div>
                }
                <div className="suse">
                    <div className="img"></div>
                    <div className="img-body">
                        <h1>SUSE Linux Enterprise Server</h1>
                        <p>Linux 2 comes with the 5years of support.It provides the linux kernal 4.14 tuned for optimal perfrmance</p>
                    </div>
                    <div className="img-processor">
                        <div><input type="radio" name="img-processor" onClick={() => { onRadioClick("x86") }} /> 64-bit (x86)</div>
                        <div><input type="radio" name="img-processor" onClick={() => { onRadioClick("ARM") }} /> 64-bit (ARM)</div>
                        <button onClick={() => { onSelect("SUSE Linux Enterprise Server", processorLabel) }} className="select-button">select</button>
                    </div>
                </div>
                <div>
                    <button className="proceed-button" onClick={() => { props.POnefunction(cardDetails) }}  >Proceed</button>
                </div>
                
            </div>
                
                
        </div>

    );
};


