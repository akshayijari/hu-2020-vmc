import * as React from 'react';
import { render } from '@testing-library/react'

import {ImageList} from '../index';

describe('Testing our ImgList component', () =>{
    test('testing Region1',() =>{
        const { getByText } = render (<ImageList region='India-1' ponecost ={()=>{}} POnefunction={()=>{}} />)
        const imgElement = getByText('Red Hat Enterprise Linux 8');
        expect(imgElement).toBeInTheDocument();
    });

    test('testing Region2',() =>{
        const { getByText } = render (<ImageList region='US-East-1' ponecost ={()=>{}} POnefunction={()=>{}} />)
        const imgElement = getByText('Microsoft Windows Server 2019 Base');
        expect(imgElement).toBeInTheDocument();
    });

    
})