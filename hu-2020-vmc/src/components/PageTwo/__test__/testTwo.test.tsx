import React from 'react';
import { render } from '@testing-library/react';
import { InstancePage } from '../index';

const options = ['4core','4GB','General Purpose'];

describe('Testing our InstancePage component', () =>{
    test('Testing General Purpose component',() =>{
        const { getByText } = render (<InstancePage cost2 ={()=>{}} ptwodata ={()=>{}} />)
        const InstanceType = getByText('General Purpose');
        expect(InstanceType).toBeInTheDocument();
    });

    test('Testing General Purpose component',() =>{
        const { getByRole } = render (<InstancePage cost2 ={()=>{}} ptwodata ={(options)=>{}} />)
        const ControlElement = getByRole('button');
       
        fireEvent.click(ControlElement);

        const CoreMenu =getByRole('coreMenu');
        const DropdownIndicator = getByRole('4core');

        expect(CoreMenu).toBeInTheDocument();
    });

    test('Testing General Purpose component',() =>{
        const { getByRole } = render (<InstancePage cost2 ={()=>{}} ptwodata ={(options)=>{}} />)
        const ControlElement = getByRole('button');
       
        fireEvent.click(ControlElement);

        const CoreMenu =getByRole('coreMenu');
        const DropdownIndicator = getByRole('16core');

        expect(DropdownIndicator).nottoBeInTheDocument();
    });

    
})