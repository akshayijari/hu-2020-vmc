import * as React from 'react';
import './style.css';
import * as DropDown from '../../constants/constants';

interface IInstanceProps {
    ptwodata: (cost: string[]) => void;
    cost2: (cost2: number) => void;
}

export const InstancePage: React.FC<IInstanceProps> = (props: IInstanceProps) => {
    const [cpuCore, setCpuCores] = React.useState('CPU Cores');
    const [cpuMemory, setCpuMemory] = React.useState('Memory');
    const [dropdownlabel, setDropdownLabel] = React.useState("General Purpose");
    const [cost1, setCost1] = React.useState(0);
    const [cost2, setCost2] = React.useState(0);


    const onCpuCoreChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        setCpuCores(event.target.value);
        { event.target.value === '8core' && setCost1( 20); }
        { event.target.value === '16core' && setCost1( 40); }
    };

    const onCpuMemoryChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        setCpuMemory(event.target.value);
        { event.target.value === '32GB' && setCost2(20); }
        { event.target.value === '64GB' && setCost2(40); }
    }

    const onDropdownChange = (dropdownlabel: string) => {
        setDropdownLabel(dropdownlabel);
    }

    const sendData = () => {
        props.cost2(cost1+cost2);
        props.ptwodata([cpuCore, cpuMemory, dropdownlabel]);
    }

    return (
        <div>
            <div className="purpose">
                <button onClick={() => { onDropdownChange("General Purpose") }} className={dropdownlabel === "General Purpose" ? 'selected' : 'general'}>General Purpose</button>
                <button onClick={() => { onDropdownChange("CPU Optimised") }} className={dropdownlabel === "CPU Optimised" ? 'selected' : 'cpu'}>CPU Optimised</button>
                <button onClick={() => { onDropdownChange("Storage Optimised") }} className={dropdownlabel === "Storage Optimised" ? 'selected' : 'storage'}>Storage Optimised</button>
                <button onClick={() => { onDropdownChange("Network Optimised") }} className={dropdownlabel === "Network Optimised" ? 'selected' : 'network'}>Network Optimised</button>
            </div>
            <div className="DD-block">
                {
                    dropdownlabel === "General Purpose" &&
                    <div className="core-memory">
                        <select role="coreMenu" className="cpu-cores" onChange={onCpuCoreChange} value={cpuCore}>
                            {DropDown.GeneralCore.map((Core: string) => (
                                <option value={Core}>{Core}</option>
                            ))
                            }
                        </select>

                        <select role="storageMenu" className="cpu-memory" onChange={onCpuMemoryChange} value={cpuMemory}>
                            {DropDown.GeneralMemory.map((Memory: string) => (
                                <option value={Memory}>{Memory}</option>
                            ))
                            }
                        </select>
                    </div>
                }
                {
                    dropdownlabel === "CPU Optimised" &&
                    <div className="core-memory">
                        <select role="coreMenu" className="cpu-cores" onChange={onCpuCoreChange} value={cpuCore}>
                            {DropDown.CPUCore.map((Core: string) => (
                                <option value={Core}>{Core}</option>
                            ))
                            }
                        </select>

                        <select role="storageMenu" className="cpu-memory" onChange={onCpuMemoryChange} value={cpuMemory}>
                            {DropDown.CPUMemory.map((Memory: string) => (
                                <option value={Memory}>{Memory}</option>
                            ))
                            }
                        </select>
                    </div>
                }
                {
                    dropdownlabel === "Storage Optimised" &&
                    <div className="core-memory">
                        <select role="coreMenu" className="cpu-cores" onChange={onCpuCoreChange} value={cpuCore}>
                            {DropDown.StorageCore.map((Core: string) => (
                                <option value={Core}>{Core}</option>
                            ))
                            }
                        </select>

                        <select role="storageMenu" className="cpu-memory" onChange={onCpuMemoryChange} value={cpuMemory}>
                            {DropDown.StorageMemory.map((Memory: string) => (
                                <option value={Memory}>{Memory}</option>
                            ))
                            }
                        </select>
                    </div>
                }
                {
                    dropdownlabel === "Network Optimised" &&
                    <div className="core-memory">
                        <select role="coreMenu" className="cpu-cores" onChange={onCpuCoreChange} value={cpuCore}>
                            {DropDown.NetworkCore.map((Core: string) => (
                                <option value={Core}>{Core}</option>
                            ))
                            }
                        </select>

                        <select role="storageMenu" className="cpu-memory" onChange={onCpuMemoryChange} value={cpuMemory}>
                            {DropDown.NetworkMemory.map((Memory: string) => (
                                <option value={Memory}>{Memory}</option>
                            ))
                            }
                        </select>
                    </div>
                }

            </div>
            <div>
                <button onClick={sendData} className="proceed-button"   >Proceed</button>
            </div>

        </div>


    );

};
