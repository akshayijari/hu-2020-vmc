import React from 'react';
import { render,fireEvent } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});

test('Testing General Purpose component',() =>{
  const { getByRole } = render (<App />)
  const ControlElement = getByRole('button');
 
  fireEvent.click(ControlElement);

  const RegionMenu =getByRole('RegionMenu');
  const DropdownIndicator = getByRole('India-1');

  expect(RegionMenu).toBeInTheDocument();
  
});